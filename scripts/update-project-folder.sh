#!/bin/bash

scripts_dir="$(dirname "$0")"
repo_root="$(dirname "$scripts_dir")"
src="${HOME}/App Builder/Scripture Apps/App Projects/Praises to the Most High"
dest="${repo_root}"

rsync -autv "$src" "$dest"
