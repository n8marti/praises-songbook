![](data/icon.png)

# Praises to the Most High Songbook

An unofficial digitization of the songbook originally published by AGLC in Nairobi, Kenya in 1993.

This repository contains:
- the source data for the songbook app
- a backup of the project files

The project files can be used with Scripture App Builder to recreate the app.
